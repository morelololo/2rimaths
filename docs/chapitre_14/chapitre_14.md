---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch14 Equations de droites
---

![](../images/ch13.png)

- [Activités](equations_de_droite_activite_geogebra.pdf) 
- [Le cours](equations_de_droites_cours.pdf) 
- [Plan de travail](Plan_de_travail_Equations_de_droites.pdf)
- [Correction Exercices ](equations_de_droite_exercices_correction.pdf)


- Vidéo :  Système de deux équations linéraires à deux inconnues[![](../images/youtube.png)](https://youtu.be/-KOVbIG_gzA)


