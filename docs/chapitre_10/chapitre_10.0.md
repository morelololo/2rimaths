---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch10  Python Boucles  Version Papier
---
# Python  (Partie 2) : Les boucles  
## Version papier


![](../images/ch6.png)



- [Le cours Boucles](Boucles.pdf)  

- [Le cours Boucles _ Correction](boucles_correction.pdf)

- [Correction Exercices](corrections_exercices.pdf)  

- [Correction TP1](https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp1)

- [Correction TP2](https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp2)

- [Correction TP3](https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp3)

- [Correction TP4](https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp4)



- Commentaires de cours en vidéo  : [![](../../images/youtube.png)]( https://youtu.be/1DnEXC1lBBQ ) 



