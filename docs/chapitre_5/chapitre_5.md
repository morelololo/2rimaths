---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch05 Géométrie repérée
---

![](../images/ch5.png)

- [Le cours ](geometrie_reperee_cours_avec_plan_de_travail_2024.pdf)
- [Le cours Correction ](geometrie_reperee_cours_avec_plan_de_travail_et_Corrections_2024.pdf)
- [Exercices : Correction](geometrie_reperee_exercices_correction.pdf)
- Exercice de synthèse en vidéo : [![](../images/youtube.png)](https://youtu.be/fhNVnFt_IGI?si=oIeSyalIVkElr9nA)

- Vidéo sur la définition d'un repère : [![](../images/youtube.png)](https://youtu.be/5j-2o28Zjr4?si=CtIX7GcGn3cG7KA_)





