---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch04 Nombres réels et intervalles
---

![](../images/ch4.png)

<iframe title="Voyages&#x20;au&#x20;pays&#x20;des&#x20;maths" allowfullscreen="true" style="transition-duration:0;transition-property:no;margin:0 auto;position:relative;display:block;background-color:#000000;" frameborder="0" scrolling="no" width="100%" height="100%" src="https://www.arte.tv/embeds/fr/097454-009-A?autoplay=true&mute=0"></iframe>


- [Le cours](nombres_reels_et_intervalles_cours 2024.pdf)
- [Le cours Correction](nombres_reels_et_intervalles_cours 2024_correction.pdf)
- [Exercices : Correction](nombres_reels_et_intervalles_exercices_correction.pdf)

  
  


