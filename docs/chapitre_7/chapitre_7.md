---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch07 Inéquations et valeurs absolues
---

![](../images/ch7.png)

- [Le cours](inequations_et_valeur_absolue_cours_avec_plan_de_travail.pdf)
- [Le cours Correction](inequations_et_valeur_absolue_cours_avec_plan_de_travail_correction.pdf)
- [animation géogébra pour la résolution d'équations et inéquations avec valeur absolue](https://www.geogebra.org/m/jfyxknha)
- Vidéo Propriétés des inégalités et encadrements: [![](../images/youtube.png)](https://youtu.be/NjpsII-lSpY)
- Vidéo Résolutions d'inéquations du premier degré: [![](../images/youtube.png)](https://youtu.be/3hlcuxaRj_Y)
- Vidéo Modélisation d'un problème géométrique par la résolution d'une inéquations du premier degré: [![](../images/youtube.png)](https://youtu.be/It1lLX2StA8)


- Valeur absolue :Commentaires sur le cours et exemples de cours  : [![](../images/youtube.png)](https://youtu.be/PzZ-TTwchGo?si=r8qLQ4scAmMHpQop)

- [animation géogébra qui illustre les propriétés des inégalités](https://www.geogebra.org/m/nvdtgapd)



Correction des exercices :

Thème|Corrections
---|---
Propriété des inégalités : 5-6-42-43-45-86  | [Correction](correction_exercices_inequations_valeur_absolue_5_6_42_43_45_86.pdf)
Manipuler des inéquations :50-51-52-53-54-55-56-57-58-59-60-91-92-94-97-98-112| [Correction](correction_exercices_inequations_valeur_absolue_50_à_60 _91_92_94_97_98_112.pdf)
Modéliser par une inéquation : 62-63-64-113 |[Correction](correction_exercices_inequations_valeur_absolue_62_63_64_113.pdf)
Valeurs absolues : 66-67-68-69-70-71-73 | [Correction](correction_exercices_inequations_valeur_absolue_66_67_68_69_70_71_73.pdf)
Inéquations et valeur absolue : 100-101-102-103-104-105-120-121 | [Correction](correction_exercices_inequations_valeur_absolue_100_101_102_103_104_105-120-121.pdf)
Problèmes à 2 inconnues : 106-107-108 | [Correction](correction_exercices_inequations_valeur_absolue_106_107_108.pdf)





