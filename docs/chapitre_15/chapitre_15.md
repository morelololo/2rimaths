---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch15 Signe d'une fonction
---


- [Correction Activités 2 du manuel](Correction_Acrivité_2_Fonctions.pdf) 
- [Le cours](CH15_signe_d_une_fonction.pdf) 
- [Plan de travail](Plan_de_travail.pdf)
- [Correction Exercices parties 1 à 5 ](Correction_exercices_Signe_d_une_fonction(Parties_1_a_5_du_plan_de_travail).pdf)
- [Correction Exercices parties 6 et 7 ](Correction_exercices_Signe_d_une_fonction(Parties_6_et_7_du_plan_de_travail).pdf)
- [DM Entreprise de surf](Entreprise_surf.pdf)
- [DM Entreprise de surf Correction](Correction_DM_Entreprise_surf.pdf)
- [Fiche méthodologique Equations Inéquations](Fiche_methodo_inequations.pdf)



- Vidéo :  Signe d'une fonction , signe d'une fonction affine [![](../images/youtube.png)](https://youtu.be/MMkFCLCCMpo )

- Vidéo :  Signe d'un produit , signe d'un quotient [![](../images/youtube.png)]( https://youtu.be/HOdqrFC70yg  )

- Vidéo :  Problème se ramenant à une inéquation Produit ou quotient [![](../images/youtube.png)](  https://youtu.be/KvuNrhSPXWY   )