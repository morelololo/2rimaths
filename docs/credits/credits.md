---
author: Equipe Mathématique Lycée Les Rimains
title: Crédits
---

Site destiné en particuliers aux élèves de secondes du lycée Les Rimains à Saint Malo 

Ce site est le fruit de la collaboration des l'ensemble des enseignants de mathématique du ['Lycée des Rimains'](https://www.lesrimains.org/)


Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

😀 Un grand merci à Mireille Coilhac qui a aidé à la misen en oeuvre de sites Mkdocs


😀 Un grand merci à  [Vincent-Xavier Jumel](https://forge.aeif.fr/vincentxavier) et [Vincent Bouillot](https://gitlab.com/bouillotvincent) qui ont réalisé la partie technique de ce site, et qui m'ont beaucoup aidé pour les mises à jour. Merci également à [Charles Poulmaire](https://forge.aeif.fr/cpoulmaire) pour ses relectures attentives et ses conseils judicieux.

