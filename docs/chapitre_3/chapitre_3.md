---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch03_Proportion et évolution
---

![](../images/ch3.png)

- [Le cours](Proportion_et_Evolution_Cours_2024.pdf)
- [Le cours Correction](Proportion_et_Evolution_Cours_2024_Complete.pdf)
- [Exercices : Correction](proportion_et_evolution_exercices_correction.pdf)
- Synthèse proportion : [![](../images/youtube.png)](https://youtu.be/JT-fmiknv-U)
,[ressource corrigée ](proportion_et_evolution_video_proportion_de_proportion.pdf)[![](../images/pdf.png)](proportion_et_evolution_video_proportion_de_proportion.pdf)
- Synthèse Taux d'évolution : [![](../images/youtube.png)](https://youtu.be/_uvd7o5CW3o)
,[ressource corrigée ](proportion_et_evolution_video_evolution_correction.pdf)[![](../images/pdf.png)](proportion_et_evolution_video_evolution_correction.pdf)


