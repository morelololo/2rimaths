---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch08 Notion de fonction
---

![](../images/ch8.png)

- [Le cours](notion_de_fonction.pdf)
- [Le cours Correction](notion_de_fonction_correction.pdf)
- [Le plan de travail](notion_de_fonction_plan_de_travail.pdf)
- [Correction des exercices du plan de travail](notion_de_fonction_exercices_correction.pdf)

- Vidéo :  domaine de définition , images , antécédents , résolution graphique: [![](../images/youtube.png)](https://youtu.be/mHXp2bVtD5k)
- Vidéo :  fonctions de références: [![](../images/youtube.png)](https://youtu.be/o91vNzVLSP4 )


- Animation : Notion de parité

<iframe scrolling="no" title="Illustration parité de fonction" src="https://www.geogebra.org/material/iframe/id/gtq4hz7x/width/1836/height/1456/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1836px" height="1456px" style="border:0px;"> </iframe>


 

