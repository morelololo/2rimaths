---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch09 Calcul littéral 
---

![](../images/ch9.png)

- [Le cours](calcul_litteral_cours_2025.pdf) 
- [Le cours complété](calcul_litteral_cours_2025_complete.pdf)

- [Correction des exercices du plan de travail](calcul_litteral_exercices_correction.pdf)
- [Exercices de remédiation et d'entraînement](Exercices_remediation_calcul_litteral.pdf)
- [Exercices de remédiation et d'entraînement avec la correction ](Exercices_remediation_calcul_litteral_correction.pdf)



- Vidéo :  Développer, factoriser [![](../images/youtube.png)](https://youtu.be/AwPOMw4bmjE)
- Vidéo :  Mettre au même dénominateur  [![](../images/youtube.png)](https://youtu.be/YvmC8YgfNKA)
- Vidéo :  Résoudre une équation  [![](../images/youtube.png)](https://youtu.be/iWtKcp2AHu0)

- Point méthode sur la résolution d'équations : 

![](./img/methodo_equations.png)

 
