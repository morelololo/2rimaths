---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch12 Probabilités
---

![](../images/ch12.png)

- [Le cours](probabilites_cours.pdf) 
- [Exercices](exercices.pdf)
- [Correction Exercices](corrections_exercices.pdf)


- Vidéo : Construire un fiche de synthèse de probas [![](../images/youtube.png)](https://www.youtube.com/watch?v=CGrTZDJrAus)

