---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch02_Géométrie_plane
---

![](../images/ch2.png)

- [Le cours](Géométrie_plane_Cours.pdf)
- [Le cours Correction](Géométrie_plane_Cours_Corrigé.pdf)
- [Exercices : Correction](geometrie_plane_exercices_correction.pdf)
- [Formulaire de géométrie](formulaire_geometrie.pdf)


