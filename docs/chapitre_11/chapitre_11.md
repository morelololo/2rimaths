---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch11 Vecteurs du plan
---

![](../images/ch11.png)

- [Le cours](vecteurs_du_plan.pdf) 
- [Le cours complété](vecteurs_du_plan_complete.pdf) 
- [Fiche d'exercices partie 1 ](vecteurs_partie1_exercices.pdf)
- [Correction des exercices Partie 1](correction_exercices_partie_1.pdf)
- [Fiche d'exercices partie 2 ](vecteurs_partie2_exercices.pdf)
- [Correction des exercices Partie 2](correction_exercices_partie_2.pdf)




- Vidéo :  Exercices type Partie 1 [![](../images/youtube.png)](https://youtu.be/5vAKWInstgg?si=ju9CkG05T-dSSxYh)

- Vidéo :  Point Méthode Partie 2 [![](../images/youtube.png)](https://www.youtube.com/watch?v=IUw0FIPubqU)

