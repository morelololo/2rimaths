---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch13 Variations de fonctions
---

![](../images/ch13.png)

- [Correction Activités](Activitee_livre_1_3_4_correction.pdf) 
- [Le cours](Variations_de_fonctions.pdf) 
- [Plan de travail](Plan_de_travail_Variations.pdf)
- [Correction Exercices  Parties I et II ](Variations_Exercices_Correction_Partie_I_et_II.pdf)
- [Correction Exercices  Parties III](Variations_Exercices_Correction_Partie_III.pdf)
- [Correction Exercices  Parties IV](Variations_Exercices_Correction_Partie_IV.pdf)
- [TP balayage](TP_Balayage.pdf)



- Vidéo :  Comparer deux nombres à l'aide des fonctions de références [![](../images/youtube.png)](https://youtu.be/gwCWYaUq1qo)
- Vidéo :  Variations des fonctions de référence résumé  [![](../images/youtube.png)](https://youtu.be/Kh5ZX7jDP5Q)
- Vidéo :  Variation de fonctions  Fonctions affines [![](../images/youtube.png)](https://youtu.be/b-xro5wgAQk)
- Vidéo :  Variation de fonctions  Fonction carré [![](../images/youtube.png)](https://youtu.be/VbVKLhYGE5c)
- Vidéo :  Variation de fonctions  Fonction inverse [![](../images/youtube.png)](https://youtu.be/2dxfQtm8hhM)

