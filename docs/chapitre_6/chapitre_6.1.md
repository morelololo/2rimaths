---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch06.1 Python variables
---

# Les Bases de programmation en Python

## I. Quels outils pour programmer en Python

### 1) La calculatrice Numworks

La calculatrice permet de programmer en Python. Pour cela , rien de plus simple :

- allez dans le menu Python

![Le menu python](img/num_menu_python.png)

- Pour créer un programme , cliquez sur ajouter un  script :

![script python](img/num_script.png)

- Pour accéder à la console Python sans créer de programme :

![console python](img/num_console.png)

### 2) En ligne avec sur le site Bosthon

Sur le site Bosthon, vous pouvez programmer directement en Python :

![Basthon](img/Basthon.png)

[accéder à Basthon ](https://console.basthon.fr/)

- Zone de gauche pour écrire un programme
- Zone de droite pour lancer des commande dans la console

### 3) Programmes installés sur l'orginateur : ThonnyPython ou Vscodium

Vous pouvez enfin installer un programme sur votre ordinateur.

Les programmes privilégiés dans l'établissement seront :

- ThonnyPython :

![ThonnyPython](img/thonny.png)

- Vscodium :

![VsCodium](img/vscodium.png)

## II. Variables et types de variables

### 2.1 Définition

On peut considérer les variables en Python comme une boite contenant un objet.
Par exemple , si l'on veut placer le nombre 1 dans une variable appelée a , on saisira `a = 1 `

![Ma première variable](img/img1.png)

!!! exemple "Exemple d'affectation"


    Dans la console saisir :

    ```pycon
    >>> a = 1
    >>> print(a)

    ```

    {{ terminal() }}


!!! info "Variables"

    Vous venez de faire trois choses en même temps :

    * Vous avez défini une variable que vous avez nommé `a`
    * Vous avez affecté la valeur 1 à `a`
    * Vous venez d'afficher le contenu de la variable `a`


### 2.2. Les affectations

!!! exemple "Exemple d'affectation"

    Dans la console saisir :

    ```pycon
    >>> a = 5
    >>> b = 3
    >>> a = a + b
    >>> print(a)
    ```

    {{ terminal() }}


!!! info "Affectation"

    Comme vous pouvez le constater, la variable b est inchangée et la commande ` a = a + b` s'explique de la manière suivante : 
    - On évalue le résultat de `a +b` et on place le résultat obtenu dans la variable a .
    - On peut penser en lisant de gauche à droite : dans `a` je place la valeur correspond à `a + b`.


!!! exemple "Avec une seule variable"

    Devinez ce qu'affichera la console après les commandes suivantes : 

    ```pycon
    >>> a = 1
    >>> a = a + 2
    >>> a = a + 3
    >>> a = a + 4
    >>> a = a + 5
    >>> print(a)
    ```
    {{ terminal() }}


### 2.3. Les types de variables

#### 2.3.1 Les chaînes de caractères : str

Les chaînes de caractères correspondent en python au type `str`( string)`.

Pour représenter une chaînes de caractères, il suffit de mettre le texte entre guillemets.

La fonction `type(variable)` permet de déterminer le type d'une variable.

!!! exemple "Exemples de chaînes de caractères"

    Dans la console saisir :

    ```pycon
    >>> a = "toto"
    >>> print(type(a))
    >>> b = "2"
    >>> print(type(b))
    >>> c = "3"
    >>> print(type(c))
    >>> print(b+c)
    ```
    {{ terminal() }}

    Comment expliquer le dernier affichage :

    * 
    *  


#### 2.3.2 Les entiers ( int) et les flottants ( float)

* Les entiers relatifs en Python, correspondent au type `int` ( integer)
* Les nombres à virgules ( virgule flottante) en python correspondent au type `float` ( float)

!!! exemple " Utilisation des entiers et des flottants"


    Deviner ce qui s'affichera dans la console en saisissant les instructions suivantes. Testez pour vérifier.
    ```pycon
    >>> a = 2
    >>> b = "2"
    >>> c = 3.0
    >>> print(type(a))
    >>> print(type(b))
    >>> print(type(c))
    >>> d = a+b
    >>> d = a + c 
    >>> print(type(d))
    ```
    {{ terminal() }}

    Comment expliquer les 3 dernières commandes ?
    * 

    * 

    * 


!!! info "types"
    Attention aux types des différentes variables  pour effectuer des opérations avec celles-ci !

**Opérations de base**

* Somme : `2+3` est égal à la somme de 2 et de 3
* Produit : `2*(-4)` est égal au  produit de 2 et de -4
* Quotient : `3/5` est égal au  quotient de 3 par 5
* Puissance : `2**4` est égal à 2 à la puissance 4
* Racine carrée (sqrt)  :

```python  
from math import sqrt # on importe la fonction sqrt dans la librairie math
  
sqrt(81) # calcule la racine carrée de 81
``` 
  

!!! info " Remarque "
    Vous rencontrez parfois des importations de librairie différentes :

    `from math import * ` charge en mémoire toutes les fonctions de la librairie math , y compris la fonction sqrt 


* Division euclidienne  : On considère la division euclidienne de 17 par 3
* `17//3` retourne 5 (le quotient )
* `17%3` retourne 2 ( le reste)

!!! exemple " Opérations entre  entiers et entre  flottants"


    Deviner ce qui s'affichera dans la console en saisissant les instructions suivantes. Testez pour vérifier.

    ```pycon
    >>> a = 2.0
    >>> b = -3
    >>> c = 4
    >>> d = 2023
    >>> print(a+b)
    >>> print(a*b)
    >>> print(a**c)
    >>> print( a**2-(b*c))
    >>> print(d % 2)
    >>> print(d//2)
    >>> from maths import sqrt
    >>> print(sqrt(3**2+4**2))

    ```
    {{ terminal() }}


???+ question "Exercice"


    === "Énoncé"
        Quel est le contenu des variables a,b, c après avoir saisi les instructions suivantes : 

        ```pycon
        >>> a = 2
        >>> b = 5
        >>> c = -1 
        >>> a = b + 2
        >>> b = c - 2
        >>> c = 3*b -1 
        >>> a = (c+1)**2 
        ```
        Vous pouvez vous aider du tableau ci- dessous pour compléter une ligne du tableau par instruction.

        a | b | c 
        ---|---|---
        ||
        ||
        ||
        ||
        ||
        ||
        ||

            
        Testez dans la console pour vérifier 

        {{ terminal() }}

    === "Correction"
        a | b | c 
        ---|---|---
        2||
        |5|
        ||-1
        7||
        |-3|
        ||-10
        81||


???+ question "Exercice2"

    === "Énoncé"
        Quel est le contenu des variables a,b, c après avoir saisi les instructions suivantes : 

        ```pycon
        >>> a = 80
        >>> b = 20
        >>> c = a 
        >>> a = b
        >>> b = c 
        ```
        Vous pouvez vous aider du tableau ci- dessous pour compléter une ligne du tableau par instruction.

        a | b | c 
        ---|---|---
        ||
        ||
        ||
        ||
        ||

        
        Testez dans la console pour vérifier 

        {{ terminal() }}

    === "Correction"
        a | b | c 
        ---|---|---
        80||
        |20|
        ||80
        20||
        |80|


#### 2.3.3 Les booléens ( True, False)

Une expression booléenne est une expression dont le résultat  prendra deux valeur possibles : `True` ou `False`

Opérations sur les booléens :

- test d'égalité  : `a==b` ( savoir si a est égal à b)
- test d'inégalité : `a!=b` ( savoir si a est différent de b)
- test de comparaison : `a<b` ,`a<=b`, `a>b`, `a<=b`
- opérateurs and : `a and b` (vrai si et seulement si  a et b sont vrais)
- opérateurs or : `a or b` (vrai si et seulement si  au moins l'un des deux est vrai)
- opérateur not : `not (a)` (vrai si et seulement a est faux)

!!! exemple " Utilisation des Booléens"


    Deviner ce qui s'affichera dans la console en saisissant les instructions suivantes. Testez pour vérifier.
    ```pycon
    >>> print(2>3)
    >>> print(3>-1)
    >>> print(2==2)
    >>> print(2 !=2)
    >>> print(3>3)
    >>> print(2<=2)
    >>> a = True
    >>> b = False
    >>> c = True
    >>> d = False
    >>> print(a and b)
    >>> print(a or b)
    >>> print((a or b) and c)
    >>> print(not(c) and d)
    ```
    {{ terminal() }}


!!! info "Booléens"

   Les Booléens seront très utilisés par la suite dans les structures conditionnelles et les boucles


### 3.4 Conversion de type de variable

Lorsque c'est cohérent, il est possible de convertir une expression d'un type à un autre.

Par exemple il est possible de convertir un flottant en str :

```pycon
form math import sqrt
a = 2.34
b = sqrt(a)
print(type(a))
print(b)
print(type(b))
```

Dans les instructions ci-dessus, a est de type float et on affecte à b la conversion de a en str .

!!! exemple " Exemple de conversion"
    Deviner ce qui s'affichera dans la console en saisissant les instructions suivantes. Testez pour vérifier.


    ```pycon
    >>> a = srt(-6)
    >>> print(a, type(a))
    >>> b = int(2.5)
    >>> print(b, type(b))
    >>> c = float(5)
    >>> print(c, type(c))
    >>> d = float("toto")
    ```

    {{ terminal() }}

    Expliquez ce permettent de faire les instructions 1,3,5 et 7 

    *

    *

    *

    *

## IV. Interaction

### 4.1 Affichage

Comme vous avez déjà pu le voir dans le début de ce chapitre , la commande permettant d'afficher une information dans la console est `print()`

Pour afficher plusieurs informations, il suffit d'utiliser la virgule comme séparateur.

!!! exemple "Affichage"


    ```pycon
    a = "Bonjour Tom, tu as "
    b = 15
    c = " ans." 
    print(a , b , c)

    ```
    Tester les instructions ci-dessous dans la console :

    {{ terminal() }}


### 4.2 Interrogation par la console

Dans de nombreux programmes, nous seront amenés à demander à l'utilisateur de saisir une information . La commande permettant d'effectuer cette interrogation est `input()`. L'information saisie sera enregistrée dans le type `str`.

!!! exemple "Interrogation dans la console"
    Analysez les instructions suivantes et devinez ce que la console affichera :


    ```pycon
    >>> age  =  input(" donnez votre age :")
    >>> print("type(age)")
    >>> print( " Bonjour, vous avez ", age, "ans)
    >>> print( " Vous êtes né en ", 2023 - age)
    ```

    {{ terminal() }}

    La dernière instruction renvoie une erreur. Comment modifier le code pour qu'elle afficher votre date de naissance ?