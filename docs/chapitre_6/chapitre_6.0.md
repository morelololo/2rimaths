---
author:  Equipe Mathématique Lycée Les Rimains
title: Ch06 Python Version Papier
---
# Python : Partie 1


![](../images/ch6.png)

- [Le cours _Variables](python_variables3.pdf)  

- [Le cours _Variables _ Correction](Python_variables3.correction.pdf)

- [Le cours _Fonctions](Python_fonctions.pdf)  

- [Le cours _Fonctions _ Correction](Python_fonctions.correction.pdf)

- [Le cours _Structures conditionnelles](Python_structures_conditionnelles.pdf)  

- [Le cours _Structures conditionnelles _ Correction](Python_structures_conditionnelles.correction.pdf)

- Synthèse Python ( variables et conditionnelles) : [![](../../images/youtube.png)]( https://youtu.be/yLtB5nS4jsM ) 

- Synthèse Python ( fonctions ) : [![](../../images/youtube.png)]( https://youtu.be/nYA7AEjMHZg?si=DcYFg51k5HzJmxJx ) 






