def donner_categorie(age):

    if age >= 6 and age <= 7 :
        return "Poussin"
    elif age >= 8 and age <= 9 :
        return "Pupille"
    elif age >= 10 and age <= 11 :
        return "Minime"
    elif age >= 12 :
        return "Cadet"
          