# 2rimaths : Les mathématiques en seconde aux rimains
## Introduction

Ce site est destiné aux élèves de seconde générale  en mathématiques au Lycée Les Rimains à Saint-Malo

Sur ce site vous trouverez tout ce dont vous avez besoin pour avancer dans votre travail en autonomie : 

- La progression de l'année 
- Des liens vers des ressources exterieures 
- Et pour chaque chapitre :
    - La fiche de cours et sa correction avec sa vidéo 
    - Une fiche de synthèse  avec sa vidéo 
    - Un plan de travail pour le chapitre
    - La correction des exercices du plan de travail

## Progression
| Chapitre | Titre | Temps |
| :---    | :----:    | ---:   |
|1|Calcul numérique ( fractions- puissance- racine carré)|
|2|Rappels de géométrie|
|3|Pourcentage et évolution|
|4|Calcul littéral et équations du premier degré|
|5|Géométrie repérée |
|6|Algorithmes et Python  ( Variables et boucles)|
|7|Intervalle et Inéquations du premier degré|
|8|Généralités sur les fonctions et Fonctions de références|
|9|Algorithmes et Python  ( Fonctions )|
|10|Vecteurs |
|11|Probabilités|
|12|Variation de fonctions |
|13|Droites et équations de droites|
|14|Signe d’une fonction|
|15|Statistiques – Échantillonnage  |



## Le manuel

Nous travaillons avec le manuel séasamath : 

[![](images/sesamaths.png)](https://manuel.sesamath.net/numerique/?ouvrage=ms2_2019)

Pour vous aider à vous positionner : 

[![](images/sesamaths_cahier.png)](https://manuel.sesamath.net/numerique/index.php?ouvrage=cah2nde_2022&page_gauche=1)


## La calculatrice 

Nous utilisons la calculatrice Numworks : 
 
[![](images/numworks.png)](https://www.numworks.com/fr/simulateur/)
