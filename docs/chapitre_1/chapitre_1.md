---
author: Equipe Mathématique Lycée Les Rimains
title: Ch01_Calcul numérique
---

![](../images/ch1.jpg)

- [Le cours](calcul_numerique_cours_a_trous.pdf)
- [Le cours Correction](calcul_numerique_cours.pdf)
- [Plan de travail](calcul_numerique_plan_de_travail.pdf)
- [Exercices : Sujets](calcul_numerique_exercices.pdf)
- [Exercices : Correction](calcul_numerique_exercices_corrections.pdf)

- Synthèse fractions : [![](../images/youtube.png)](https://youtu.be/pVxeZWe-al4) , [ressource ](video_rappel_sur_les_fractions.pdf)[![](../images/pdf.png)](video_rappel_sur_les_fractions.pdf)
,[ressource corrigée ](video_rappel_sur_les_fractions_correction.pdf)[![](../images/pdf.png)](video_rappel_sur_les_fractions_correction.pdf)

- Synthèse puissances : [![](../images/youtube.png)](https://youtu.be/EoP3y_GphD8) 
,[ressource corrigée ](video_rappel_sur_les_puissances_correction.pdf)[![](../images/pdf.png)](video_rappel_sur_les_puissances_correction.pdf)

- Synthèse Racines : [![](../images/youtube.png)](https://youtu.be/9IsND2u4EW8) 
,[ressource corrigée ](video_rappel_sur_les_racinces_correction.pdf)[![](../images/pdf.png)](video_rappel_sur_les_racinces_correction.pdf)


- Devoir Maison 1 : [Sujet](DM1_calcul numérique.pdf)  [Correction Partielle](DM1_calcul numérique.correction.pdf)

